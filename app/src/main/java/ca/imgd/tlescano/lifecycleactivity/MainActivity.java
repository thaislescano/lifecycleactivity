package ca.imgd.tlescano.lifecycleactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity
{
  final String TAG ="LIFE";
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Log.e(TAG, "onCreate");
  }

  @Override
  protected void onStart()
  {
    super.onStart();
    Log.w(TAG, "onStart");
  }

  @Override
  protected void onResume()
  {
    Log.v(TAG, "onResume");
  }

  @Override
  protected void onPause()
  {
    Log.e(TAG, "onPause");
  }

  @Override
  protected void onStop()
  {
    Log.e(TAG, "onStop");
  }

  @Override
  protected void onRestart()
  {
    Log.w(TAG, "onRestart");
  }

  @Override
  protected void onDestroy()
  {
    Log.e(TAG, "onDestroy");
  }
}
